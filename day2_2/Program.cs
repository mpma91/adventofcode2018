﻿using System;
using System.IO;
using System.Linq;

namespace day2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] boxIds = File.ReadAllLines(".\\input.txt");
            int i = 0;
            int j = 0;
            bool found = false;

            while (!found)
            {
                for (i = 0; i < boxIds.Length; i++)
                {
                    j = 0;
                    foreach(string boxId in boxIds)
                    {
                        if (DifferByOneChar(boxIds[i], boxId))
                        {
                            Console.WriteLine($"{boxIds[i]} and {boxId} differs by one char");
                            found = true;
                            break;
                        }

                        j++;
                    }

                    if (found)
                        break;
                }
            }

            Console.WriteLine("Be a man and read the result yourself, im not your slave... hooman");
            Console.WriteLine(boxIds[i]);
            Console.WriteLine(boxIds[j]);
        }

        static bool DifferByOneChar(string a, string b)
        {
            char[] a_letters = a.ToCharArray();
            char[] b_letters = b.ToCharArray();
            int len = a_letters.Length;
            int diffCount = 0;

            for (int i = 0; i < len; i++)
            {
                if (diffCount > 1)
                    break;
                
                if (a_letters[i] != b_letters[i])
                    diffCount++;
            }

            return diffCount == 1;
        }

        static bool DiffersByTwoChars(char[] i_letters, char[] j_letters)
        {
            int differCount = 0;

            foreach (char i_letter in i_letters)
            {
                foreach (char j_letter in j_letters)
                {
                    Console.WriteLine($"{i_letter} == {j_letter} ?");
                    if (!i_letter.Equals(j_letter))
                        differCount++;

                    Console.WriteLine("Differ count: " + differCount);
                }
            }

            return differCount == 1;
        }
    }
}
