﻿using System;
using System.IO;

namespace day1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int frequency = 0;
            string[] frequencyChanges = File.ReadAllLines(".\\input.txt");

            foreach (var frequencyChange in frequencyChanges)
                frequency += int.Parse(frequencyChange);

            Console.WriteLine($"Resulting frequency: {frequency}");
        }
    }
}
