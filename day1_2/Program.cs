﻿using System;
using System.Collections.Generic;
using System.IO;

namespace day1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentFrequency = 0;
            List<int> uniqueFrequencies = new List<int>();
            string[] frequencyChanges = File.ReadAllLines(".\\input.txt");
            bool repeatFrequencyFound = false;

            while (!repeatFrequencyFound)
            {
                foreach (string frequencyChange in frequencyChanges)
                {
                    if (uniqueFrequencies.Contains(currentFrequency))
                    {
                        repeatFrequencyFound = true;
                        break;
                    }

                    uniqueFrequencies.Add(currentFrequency);
                    currentFrequency += int.Parse(frequencyChange);
                }
            }

            Console.WriteLine($"First repeat frequency is {currentFrequency}");
        }
    }
}
