﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace day2_1
{
    class Program
    {
        static bool ContainsTwoOfAKind(string input)
        {
            // check each letter, if it exists twice, store it and do not check it again
            char[] letters = input.ToCharArray();
            IDictionary<char, int> twoOfAKind = new Dictionary<char, int>();

            foreach (char letter in letters)
            {
                if (twoOfAKind.ContainsKey(letter))
                {
                    twoOfAKind[letter] += 1;
                    continue;
                }

                twoOfAKind.Add(letter, 1);
            }

            return twoOfAKind.Any(x => x.Value == 2);
        }

        static bool ContainsThreeOfAKind(string input)
        {
            char[] letters = input.ToCharArray();
            IDictionary<char, int> threeOfAkind = new Dictionary<char, int>();

            foreach (char letter in letters)
            {
                if (threeOfAkind.ContainsKey(letter))
                {
                    threeOfAkind[letter] += 1;
                    continue;
                }

                threeOfAkind.Add(letter, 1);
            }

            return threeOfAkind.Any(x => x.Value == 3);
        }

        static void Main(string[] args)
        {
            string[] boxIDs = File.ReadAllLines(".\\input.txt");
            int twoOfAKind = 0;
            int threeOfAKind = 0;

            foreach (string boxID in boxIDs)
            {
                if (ContainsTwoOfAKind(boxID))
                    twoOfAKind++;

                if (ContainsThreeOfAKind(boxID))
                    threeOfAKind++;
            }

            Console.WriteLine($"Two of a kinds: {twoOfAKind}");
            Console.WriteLine($"Three of a kinds: {threeOfAKind}");
            Console.WriteLine($"Checksum: {twoOfAKind * threeOfAKind}");
        }
    }
}
